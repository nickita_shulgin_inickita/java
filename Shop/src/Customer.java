import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Customer {

	private String name;
	private int id;
	static AtomicInteger nexId = new AtomicInteger();
	private LinkedList<Product> products;
	
	
	public Customer(String name) {
		this.id = nexId.incrementAndGet();
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Customer [name=" + name + ", id=" + id + "]";
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LinkedList<Product> getProducts() {
		return products;
	}
	public void setProducts(LinkedList<Product> products) {
		this.products = products;
	}
	
	
	
	
}
