import java.util.LinkedList;

public class Launcher {
private static Shop shop;

	public static void main(String[] args) {

		shop = new Shop("Dick Smith");
		shop.addCustomer(new Customer("Nick"));
		shop.addCustomer(new Customer("Vitaly"));
		shop.addCustomer(new Customer("Vitaly2"));
		
		for(Customer customer: shop.getCustomers()){
			System.out.println(customer);
		}
		
	}

}
