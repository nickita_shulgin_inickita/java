import java.util.*;

public class Shop {
	
private LinkedList<Product> products = new LinkedList<Product>();
private LinkedList<Customer> customers = new LinkedList<Customer>();
private String name;

public Shop(String name){
	this.name = name;
}

public void addCustomer(Customer customer){
	customers.add(customer);
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public LinkedList<Product> getProducts() {
	return products;
}

public void setProducts(LinkedList<Product> products) {
	this.products = products;
}

public LinkedList<Customer> getCustomers() {
	return customers;
}

public void setCustomers(LinkedList<Customer> customers) {
	this.customers = customers;
}


	


}
